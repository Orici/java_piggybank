/*
 * Respuesta a pregunta:
 * https://es.stackoverflow.com/questions/143789
 *
 * Moisés Alcocer, 2018
 * https://www.ironwoods.es
 */
package Hucha;

class Coin {

    private final int value;
    private int quantity = 0;   //Number of coins

    /**
     * Construct
     *
     * @param value
     */
    public Coin( int value ) {
        this.value = value;
    }

    /**
     * Construct
     *
     * @param value
     * @param quantity
     */
    public Coin( int value, int quantity ) {
        this.value = value;
        this.quantity = quantity;
    }

    /**
     * Gets the value of value
     *
     * @return int
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Sets the value of value
     *
     * @return int
     */
    public void addQuantity( int quantity ) {
        this.quantity = this.quantity + quantity;
    }

    /**
     * Sets the value of value
     *
     * @return int
     */
    public void delQuantity( int quantity ) {
        this.quantity = this.quantity - quantity;
    }

    /**
     * Sets the value of value
     *
     * @return int
     */
    public int getQuantity() {
        return quantity;
    }

} //class
