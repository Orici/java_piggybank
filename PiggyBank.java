/*
 * Respuesta a pregunta:
 * https://es.stackoverflow.com/questions/143789
 *
 * Moisés Alcocer, 2018
 * https://www.ironwoods.es
 */
package Hucha;


class PiggyBank {

    /*******************************************************************
    *** Prperties declarations
    ***
    **/

        private Coin[] arrCoins; //Piggy Bank content (after inizialization)
        private Coin c1, c2, c5, c10, c20, c50, c100, c200;
        private int[] coinValues;
        private int[] moneyAmountsInEachCoin;
        private int coinTypes;


    /*******************************************************************
    *** Constructors
    ***
    **/

        /**
         * Construct
         *
         */
        public PiggyBank() {
            init();
        }


    /*******************************************************************
    *** Public methods
    ***
    **/

        /**
         * Adds a quantity of money of the Piggy Bank
         *
         * @param  quantity
         */
        public void addMoney( float quantity ) {
            Utils.trace( "\nAñadiendo dinero: " + Utils.getCoinFormat( quantity * 100 ) + "€" );
            //Utils.trace( "Total: " + getTotalAmount() + "€" );
            int[] coins = assignQuantityToEachCoin( quantity );

            int i = 0;
            for ( Coin coin : arrCoins ) {
                if ( coins[ i ] > 0 ) {
                    //Utils.trace( "Contenido previo: " + coin.getQuantity( ));
                    coin.addQuantity( coins[ i ]);
                    //Utils.trace( "Contenido actual: " + coin.getQuantity( ));
                }

                i++;
            }
            updateAmounts();
            //Utils.trace( "\nTotal: " + getTotalAmount() + "€" );
        }

        /**
         * Gets the content in the Piggy Bank
         *
         * @return Coin[]
         */
        public Coin[] getContent() {

            return this.arrCoins;
        }

        /**
         * Gets the total of money in the Piggy Bank
         *
         */
        public int getTotalAmount() {

            int totalAmount = 0;
            for ( int amount : moneyAmountsInEachCoin ) {
            //for( int i = 0; i<coinTypes; i++ ) {
            //    int amount = (int) moneyAmountsInEachCoin[ i ];
                totalAmount += amount;
            }

            return totalAmount;
        }

        /**
         * Searchs a quantity of money in the PiggyBank
         *
         * @param quantity
         * @return
         */
        public boolean hasMoney( int quantity ) {

            return ( getTotalAmount() > quantity );
        }

        /**
         * Shows the PiggyBank content
         *
         */
        public static void showContent( Coin[] arrCoins ) {

            int quantity, totalValue, value;
            for(Coin c : arrCoins) {

                value      = c.getValue();
                quantity   = c.getQuantity();
                totalValue = value * quantity;

                System.out.println(
                    "Hay " + quantity
                    + " monedas de " + value
                    + ". Con un valor de: "
                    + Utils.getCoinFormat( totalValue )
                    + "€" );
            }
        }


    /*******************************************************************
    *** Private methods
    ***
    **/

        /**
         * Takes out a quantity of money of the Piggy Bank
         *
         * @param  float        quantity
         * @return float
         */
        public float takeMoney( float quantity ) {

            int money = (int) ( quantity * 100 );
            Utils.trace( "Retirando " + money + " cents..." );

            if ( hasMoney( money )) {
                sustractMoney( money ); // Receive cents

                return money;
            }

            System.err.println(
                "ERR -> no hay suficiente dinero. \nDisponible: "
                + getTotalAmount( ));

            return 0;
        }

        /**
         * Gets the quantity for each coin
         *
         * @param  int  quantity
         * @return Coin[]
         */
        private int[] assignQuantityToEachCoin( float quantity ) {

            //Rounds to 2 decimal to prevent errors with incorrect entries
            quantity = (float) (Math.round(quantity * 100.0) / 100.0);

            //Transform to cents // Example: 1,23 => 123
            quantity = quantity * 100;

            int[] coins = new int[coinTypes];
            int nCoins = 0;
            int value;

            for ( int i = ( coinTypes - 1 ); i >= 0; i-- ) {
                //Obtiene el valor para la moneda, del mayor al menor
                value = coinValues[i];

                //Cantidad de dinero mayor al valor de la moneda:
                //Obtener el número de esa moneda
                if ( quantity > value ) {
                    //Obtiene número de monedas
                    nCoins = (int) quantity / value;

                    //Obtiene resto de dinero
                    quantity = quantity - (nCoins * value);
                }

                //store the coins and reset number
                coins[i] = nCoins;
                nCoins = 0;
            }

            return coins;
        }

        /**
         * Puts some coins into the Piggy Bank and inits app necessary data
         *
         */
        private void init() {
            coinValues = new int[]{ 1, 2, 5, 10, 20, 50, 100, 200 };

            c1   = new Coin( coinValues[0], Utils.getRandomNumber( ));
            c2   = new Coin( coinValues[1], Utils.getRandomNumber( ));
            c5   = new Coin( coinValues[2], Utils.getRandomNumber( ));
            c10  = new Coin( coinValues[3], Utils.getRandomNumber( ));
            c20  = new Coin( coinValues[4], Utils.getRandomNumber( ));
            c50  = new Coin( coinValues[5], Utils.getRandomNumber( ));
            c100 = new Coin( coinValues[6], Utils.getRandomNumber( ));
            c200 = new Coin( coinValues[7], Utils.getRandomNumber( ));

            arrCoins  = new Coin[]{ c1, c2, c5, c10, c20, c50, c100, c200 };
            coinTypes = coinValues.length;


            //Stores amounts for each type of coin
            updateAmounts();
        }

        /**
         * Sustracts a quantity of the total content in the Piggy Bank
         *
         * @param int       amountToSustract
         */
        private void sustractMoney( int amountToSustract ) {

            int amountStored = getTotalAmount();
            arrCoins = CoinsExtractor.get(
                amountToSustract,
                amountStored,
                this.arrCoins
            );

            updateAmounts();
        }

        /**
         * Updates the values for each coin
         *
         */
        private void updateAmounts() {

            moneyAmountsInEachCoin = new int[]{
                c1.getValue()   * c1.getQuantity(),
                c2.getValue()   * c2.getQuantity(),
                c5.getValue()   * c5.getQuantity(),
                c10.getValue()  * c10.getQuantity(),
                c20.getValue()  * c20.getQuantity(),
                c50.getValue()  * c50.getQuantity(),
                c100.getValue() * c100.getQuantity(),
                c200.getValue() * c200.getQuantity()
            };
        }

} //class
