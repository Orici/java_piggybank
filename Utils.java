/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hucha;

import java.text.DecimalFormat;
import java.util.Random;

/**
 *
 * @author orici
 */
class Utils {

    /**
     * Gets a formated string with at least one entire and two decimals
     * For example: 0.50
     * Must receive a quantity in cents, for example 272, NO euros: 2,72
     *
     * @param  float        quantity
     * @return String
     */
    public static String getCoinFormat( float quantity ) {

        float res = quantity / 100;
        DecimalFormat df = new DecimalFormat( "0.00" );

        return df.format( res );
    }

    public static int getRandomNumber() {
        int randomNumber = 0;

        Random rand = new Random();
        randomNumber = rand.nextInt(50); //0 a 50
        //System.out.println( "Generated: " + randomNumber );

        return randomNumber;
    }

    public static int getRandomNumber( int n ) {
        int randomNumber = 0;

        Random rand = new Random();
        randomNumber = rand.nextInt( n ); //0 a n
        //System.out.println( "Generated: " + randomNumber );

        return randomNumber;
    }

    public static void trace( String str ) {
        System.out.println( str );
    }

} //class
