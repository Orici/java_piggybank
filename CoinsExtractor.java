/*
 * Respuesta a pregunta:
 * https://es.stackoverflow.com/questions/143789
 *
 * Moisés Alcocer, 2018
 * https://www.ironwoods.es
 */
package Hucha;

class CoinsExtractor {

    private static int remainingAmount;
    private static int resto = 0;


    /**
     * Gets the Piggy Bank content (quantity of each Coin) updates
     * after extract money
     *
     * @return int
     */
    public static Coin[] get(
        int amountToSustract,
        int totalAmount,
        Coin[] arrCoins
    ) {
        Utils.trace( "CoinsExtractor / get()" );

        PiggyBank.showContent( arrCoins ); //trace
        Utils.trace( "----------------------------" );
        //Utils.trace( "Contenido total: " + totalAmount );
        //Utils.trace( "A extraer: " + amountToSustract );
        /**
         * Returns all the coins stored in the Piggy Bank
         *
         */
        if ( amountToSustract >= totalAmount ) {

            return emptyCoins( arrCoins );
        }

        /**
         * Returns rest of the coins stored in the Piggy Bank after
         * proceeded to subtraction
         *
         */
        int coinCounter = arrCoins.length;
        Coin[] res  = new Coin[ coinCounter ];

        Utils.trace( "Restando monedas..." );
        for ( int i = ( coinCounter - 1 ); i > -1; i-- ) {

            Coin c = arrCoins[ i ]; //Gets the last type of coin
            int quant = c.getQuantity();
            Utils.trace( "\nObtenidas monedas " + quant + " de " + c.getValue( ));

            //With coins of type -> will try conditions
            //
            if ( quant > 0 ) {
                res[ i ] = getCoin( amountToSustract, quant, c.getValue( ));


            //Without coins of type
            //
            } else
                res[ i ] = c; //quantity === 0


            coinCounter = i - 1; // !!! Cuidado !!!
        }

        Utils.trace( "Nuevo contenido: " );
        PiggyBank.showContent( res ); //trace
        return res;
    }

    /**
     * Gets a coin with the quantity updated after the subtraction
     * (This method will be called if exist a o more coins of this type)
     *
     * @param  int      amountToSustract
     * @param  int      quant
     * @param  int      valOfCoin
     * @return Coin
     */
    private static Coin getCoin(
        int amountToSustract,
        int quant,
        int valOfCoin
    ) {
       //Utils.trace( "CoinsExtractor / getCoin()" );

        resto = amountToSustract;

        /**
         * Example
         *
         * valOfCoin        = 200
         * centsInCoin      = 200 (1 coin of 200 cent)
         * amountToSustract = 277
         *
         * Operation:   277 - 200 = 77 (for next CoinType)
         * n
         */

        //Returns 0 coins for this type, we substracted all of them
        //
        if ( resto >= valOfCoin ) {
            //Utils.trace( "Cantidad a sustraer > o = al valor de la moneda" );

            int centsInCoin = quant * valOfCoin; //f.e. 1*200 = 200

            //Operation:   277 - 200 = 77 (for next CoinType)
            resto = resto - centsInCoin;

            int nCoins = 0; //centsInCoin / valOfCoin;
            Coin x     = new Coin( valOfCoin, nCoins ); //200, 1

            return x;
        }
        //Utils.trace( "Cantidad a sustraer menor al valor de la moneda" );

        //Returns all the coins of the type, we didn't subtract neither of them
        //
        Coin x = new Coin( valOfCoin, quant ); //200, 1

        return x;
    }

    /**
     * Emties the value in a coin
     *
     */
    private static Coin emptyCoin( Coin c ) {

        c.delQuantity( c.getQuantity( )); //Puts Coin.quantity to 0

        return c;
    }

    /**
     * Emties the values in each coin
     *
     */
    private static Coin[] emptyCoins( Coin[] arrCoins ) {
        Utils.trace( "CoinsExtractor / emptyCoins()" );

        for ( Coin c : arrCoins ) {

            c = emptyCoin( c ); //Puts Coin.quantity to 0
        }


        return arrCoins;
    }

} //class
