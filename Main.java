/*
 * Respuesta a pregunta:
 * https://es.stackoverflow.com/questions/143789
 *
 * Moisés Alcocer, 2018
 * https://www.ironwoods.es
 *
 * Problema de la hucha
 *
 * Planteamiento
 *
 * Se crea una hucha donde inicialmente hay una cantidad de dinero: dispondremos
 * de métodos para:
 *   - Mostrar el dinero disponible: cantidad total y cantidad de cada moneda
 *   - Añadir monedas
 *   - Sacar monedas*
 *   - Saber si podemos extraer cierta cantidad
 *
 * La hucha puede contener una valor total de hasta 200,00 €, no importa en que
 * monedas se reparta esta cantidad. Si hay una cantidad y al tratar de guardar
 * más dinero se superá el máximo, se llena pero queda fuera el exceso, teniendo
 * que advertir de esta circunstancia.
 * Al tratar de sacar dinero se pueden presentar varias situaciones:
 *   - Se requiere una cantidad mayor a la disponible -> devolver todo el dinero
 *  y mostrar un mensaje de error.
 *   - Se requiere una cantidad que esta disponible pero las monedas almacenadas
 *  no permiten obtenerla -> sacar la cantidad inmediatamente superior y mostrar
 *  un aviso. Ejemplo: hay 3 monedas de 5 cent y 1 de 20 cent, total 35 cent. Se
 *  quiere sacar 12 cent, por ello se devolveran las 3 monedas de 5 cent, es
 *  decir 15 cent, indicando esta circunstancia
 *   - Se requiere una cantidad disponible con el cambio exacto -> extraer
 * Al extraer o meter dinero en la hucha se actualiza el total
 *
 */
package Hucha;


public class Main {

    private static float euros = (float) 2.77;


    public static void main(String[] args) throws NullPointerException {

        //Crear la hucha
        PiggyBank pb = new PiggyBank();

        //Muestra contenido total
        showTotal( pb );

        //Si hay almenos 1 moneda de valor 1 -> muestra monedas y sumatorio de
        //cada moneda
        //if ( pb.hasMoney( 1 ))
        //    PiggyBank.showContent( c.getContent( ));

        //pb.addMoney( euros );
        //showTotal( pb );

        euros = pb.takeMoney( euros );
        Utils.trace( "Sustraido: " + (euros / 100) + "€\n" );
        showTotal( pb );
    }

    /**
     * Muestra contenido total
     *
     * @param PiggyBank     pb
     */
    public static void showTotal( PiggyBank pb ) {

        String total = Utils.getCoinFormat( pb.getTotalAmount( ));
        Utils.trace( "Total en la hucha: " + total + "€\n" );
    }

} //class
